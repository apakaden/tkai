from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point


def get_nearest_driver_from_lat_lng(drivers, lat, lng):
    return drivers.annotate(
            distance=Distance('location', Point(lat, lng, srid=4326))
        ).order_by('distance').first()