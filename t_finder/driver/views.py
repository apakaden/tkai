import requests
from django.conf import settings
from rest_framework import generics
from .models import Driver
from .serializers import DriverSerializer
from .services import get_nearest_driver_from_lat_lng


class DriverListCreateView(generics.ListCreateAPIView):
    queryset = Driver.objects.all()
    serializer_class = DriverSerializer


class NearestDriverByLatLngDetailView(generics.RetrieveAPIView):
    serializer_class = DriverSerializer
    queryset = Driver.objects.filter(is_available=True)
    
    def get_object(self):
        lat = float(self.request.query_params['lat'])
        lng = float(self.request.query_params['lng'])

        return get_nearest_driver_from_lat_lng(self.queryset, lat, lng)


class NearestDriverByStoreDetailView(generics.RetrieveAPIView):
    serializer_class = DriverSerializer
    queryset = Driver.objects.filter(is_available=True)

    def get_object(self):
        r = requests.get(settings.INVENTORY_SERVER + '/stores/' + self.request.query_params['store_id'])
        lat = float(r.json()['lat'])
        lng = float(r.json()['lng'])

        return get_nearest_driver_from_lat_lng(self.queryset, lat, lng)