from django.urls import path
from . import views


urlpatterns = [
    path('', views.DriverListCreateView.as_view()),
    path('find_by_lat_lng/', views.NearestDriverByLatLngDetailView.as_view()),
    path('find_by_store/', views.NearestDriverByStoreDetailView.as_view()),
]