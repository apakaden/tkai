from django.contrib.gis.db import models


class Driver(models.Model):
    name = models.CharField(max_length=255)
    location = models.PointField()
    is_available = models.BooleanField()
