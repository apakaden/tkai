import React from 'react';
import axios from 'axios';

class Order extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stores: [],
            selectedStore: null,
            foods: [],
            selectedFood: null,
            driver: { id: "" }
        }
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.onSubmitForm = this.onSubmitForm.bind(this);
    }

    async getStores() {
        try {
            const response = await axios.get(
                process.env.REACT_APP_T_INVENTORY_HOST + ':' + process.env.REACT_APP_T_INVENTORY_PORT + '/stores/',
                { headers: {
                    Authorization: 'JWT ' + this.props.token,
                }}
            );
            return response.data;
        } catch (error) {
            console.log(error);
        }
    }

    async getFoods(storeId) {
        try {
            const response = await axios.get(
                process.env.REACT_APP_T_INVENTORY_HOST + ':' + process.env.REACT_APP_T_INVENTORY_PORT + '/stores/' + storeId + '/foods/',
                { headers: {
                    Authorization: 'JWT ' + this.props.token,
                }}
            );
            console.log(response);
            return response.data;
        } catch (error) {
            console.log(error);
        }
    }

    async componentDidMount() {
        const stores = await this.getStores();
        this.setState({stores});
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.selectedStore !== nextState.selectedStore) {
            return true;
        }
        if (this.state.selectedFood !== nextState.selectedFood) {
            return true;
        }
        if (this.state.foods.length === 0 || this.state.stores.length === 0) {
            return true;
        }
        if (this.state.driver.id !== nextState.driver.id) {
            return true;
        }
        return this.state.foods[0].id !== nextState.foods[0].id;
    }

    async componentDidUpdate() {
        if (this.state.selectedStore !== null) {
            const foods = await this.getFoods(this.state.selectedStore);
            this.setState({foods});
        }
    }

    handleSelectChange(field) {
        return (event) => {this.setState({[field]: event.target.value})}
    }

    renderStoreSelect() {
        return (
            <select value={this.state.selectedStore} onChange={this.handleSelectChange("selectedStore")}>
                {this.state.stores.map((value) => {
                    return <option value={value.id}>{value.name}</option>
                })}
            </select>
        )
    }

    renderFoodSelect() {
        return (
            <select value={this.state.selectedFood} onChange={this.handleSelectChange("selectedFood")}>
                {this.state.foods.map((value) => {
                    return <option value={value.id}>{value.name}</option>
                })}
            </select>
        )
    }

    async order(storeId) {
        try {
            const response = await axios.get(
                process.env.REACT_APP_T_FINDER_HOST + ':' + process.env.REACT_APP_T_FINDER_PORT + '/drivers/find_by_store/?store_id=' + storeId,
                { headers: {
                    Authorization: 'JWT ' + this.props.token,
                }}
            );
            return response.data;
        } catch (error) {
            console.log(error);
        }
    }

    async onSubmitForm(event) {
        event.preventDefault();
        const driver = await this.order(this.state.selectedStore);
        console.log(driver);
        this.setState({driver});
    }

    renderDriver() {
        return (
            <div>
                Your driver: {this.state.driver.name}
            </div>
        )
    }

    render() {
        return (
            <div>
                {this.renderStoreSelect()} <br />
                {this.renderFoodSelect()}
                <form onSubmit={this.onSubmitForm}>
                    <input type="submit" value="Order" />
                </form>
                {!!this.state.driver.id && this.renderDriver()}
            </div>
        )
    }

}

export default Order;