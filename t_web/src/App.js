import React from 'react';
import Login from './Login';
import Order from './Order';
import logo from './logo.svg';
import './App.css';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      token: null,
    };
    this.login = this.login.bind(this);
  }

  login(token) {
    this.setState({token});
  }

  render() {
    if (this.state.token === null) {
      return (
        <Login login={this.login}/>
      );
    }

    return (
      <div className="App">
        <Order token={this.state.token} />
      </div>
    );
  }
}

export default App;
