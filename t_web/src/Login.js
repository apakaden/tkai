import React from 'react';
import axios from 'axios';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
        }
        this.onInputChange = this.onInputChange.bind(this);
        this.onSubmitForm = this.onSubmitForm.bind(this);
    }

    async getToken(username, password) {
        try {
            const response = await axios.post(
                `${process.env.REACT_APP_T_AUTH_HOST}:${process.env.REACT_APP_T_AUTH_PORT}/auth/`,
                { username: username, password: password }
            );
            return response.data.token;
        } catch (error) {
            console.log(error);
        }
    }

    async onSubmitForm(event) {
        event.preventDefault();
        const token = await this.getToken(this.state.username, this.state.password);
        this.props.login(token);
    }

    onInputChange(field) {
        return (event) => {this.setState({ [field]: event.currentTarget.value})}
    }

    render() {
        return (
            <div>
                <form onSubmit={this.onSubmitForm}>
                    Username: <input type="text"
                                       value={this.state.username}
                                       onChange={this.onInputChange("username")} /><br />
                    Password: <input type="text"
                                       value={this.state.password}
                                       onChange={this.onInputChange("password")} /><br />
                    <input type="submit" value="Submit" />
                </form>
            </div>
        )
    }
}

export default Login;