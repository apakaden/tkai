#!/usr/bin/env sh

python manage.py migrate

gunicorn t_auth.wsgi:application --bind 0.0.0.0:8000 --error-logfile error.log
