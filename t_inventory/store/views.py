from rest_framework import generics
from rest_framework.permissions import AllowAny
from .models import Store, Food
from .serializers import StoreSerializer, FoodSerializer


class StoreListCreateView(generics.ListCreateAPIView):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer


class StoreDetailView(generics.RetrieveAPIView):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer
    lookup_url_kwarg = 'store_id'
    permission_classes = [AllowAny]


class FoodListCreateView(generics.ListCreateAPIView):
    serializer_class = FoodSerializer
    def get_queryset(self):
        return Food.objects.filter(store__id=self.kwargs['store_id'])