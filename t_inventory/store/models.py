from django.db import models


class Store(models.Model):
    name = models.CharField(max_length=255)
    lat = models.DecimalField(max_digits=8, decimal_places=3)
    lng = models.DecimalField(max_digits=8, decimal_places=3)


class Food(models.Model):
    name = models.CharField(max_length=255)
    price = models.IntegerField()
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
