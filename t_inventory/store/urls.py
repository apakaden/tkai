from django.urls import path
from . import views


urlpatterns = [
    path('', views.StoreListCreateView.as_view()),
    path('<int:store_id>/', views.StoreDetailView.as_view()),
    path('<int:store_id>/foods/', views.FoodListCreateView.as_view())
]