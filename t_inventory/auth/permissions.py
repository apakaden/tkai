import requests
from django.conf import settings
from rest_framework import permissions
from rest_framework.authentication import get_authorization_header


class IsAuthenticated(permissions.BasePermission):
    def get_jwt_value(self, request):
        return get_authorization_header(request).split()[1]

    def has_permission(self, request, view):
        r = requests.post(
                settings.AUTH_SERVER + '/verify/',
                data={'token':self.get_jwt_value(request)}
            )
        print(r.status_code)
        return r.status_code == 200