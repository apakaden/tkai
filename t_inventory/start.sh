#!/usr/bin/env sh

python manage.py migrate

gunicorn t_inventory.wsgi:application --bind 0.0.0.0:8088 --error-logfile error.log
